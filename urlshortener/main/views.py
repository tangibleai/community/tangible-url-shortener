from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
import base62
import json
# from urllib.parse import unquote 

def expand(request, url): 
    expanded_url = base62.decodebytes(url)
    return HttpResponseRedirect(redirect_to=expanded_url)

def index(request):
    return render(request, "index.html")

def shorten(request):
    json_body = json.loads(request.body)
    url = json_body["url"]

    return JsonResponse({
        "url": base62.encodebytes(bytes(url, "utf-8"))
        })