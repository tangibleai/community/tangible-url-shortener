# needed for docker as the image that is being used does not support cd
import os
from pathlib import Path

FILE = Path(__file__)

os.chdir(FILE.parent / "urlshortener")

os.system("python manage.py migrate")
os.system("python -m gunicorn urlshortener.wsgi:application --bind 0.0.0.0:8000")